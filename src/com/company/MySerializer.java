package com.company;

import java.io.*;
import java.lang.reflect.Field;

public class MySerializer {
    static void serialize(Object object, String file) {
        try (DataOutputStream oos = new DataOutputStream(new FileOutputStream("./" + file))) {
            Class objClass = object.getClass();
            oos.writeUTF(objClass.getName());

            Field[] fields = objClass.getDeclaredFields();
            for (Field declaredField : fields) {
                declaredField.setAccessible(true); // доступ к приватному полю
                switch (declaredField.getType().toString()) {
                    case "int": oos.writeInt((int)declaredField.get(object)); break;
                    case "double": oos.writeDouble((double)declaredField.get(object)); break;
                    case "class java.lang.String": oos.writeUTF(declaredField.get(object).toString()); break;
                }
            }
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    static Object deSerialize(String file) {
        try (DataInputStream ois = new DataInputStream(new FileInputStream("./" + file))) {
            String className = ois.readUTF();
            Class objClass = Class.forName(className);
            Object obj = objClass.newInstance();

            Field[] fields = objClass.getDeclaredFields();
            for (Field declaredField : fields) {
                declaredField.setAccessible(true);
                switch (declaredField.getType().toString()) {
                    case "int": declaredField.setInt(obj, ois.readInt()); break;
                    case "double": declaredField.setDouble(obj, ois.readDouble()); break;
                    case "class java.lang.String": declaredField.set(obj, ois.readUTF()); break;
                }
            }
            return obj;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }
}
