package com.company;

/**
 * Задание 1. Необходимо разработать класс, реализующий следующие методы:
 *
 * void serialize (Object object, String file);
 *
 * Object deSerialize(String file);
 *
 * Методы выполняют сериализацию объекта Object в файл file и десериализацию объекта из этого файла.
 * Обязательна сериализация и десериализация "плоских" объектов (все поля объекта - примитивы, или String).
 */

public class Main {

    public static void main(String[] args) {
	// write your code here
        MySerializable ms = new MySerializable(1, 2, 10.6, "str6");
        MySerializer.serialize(ms, "myObject.bin");
        Object o = MySerializer.deSerialize("myObject.bin");
        System.out.println(o);
    }
}
