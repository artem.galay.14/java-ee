package com.company;

import jdk.nashorn.internal.objects.annotations.Constructor;

import java.io.Serializable;
import java.util.Arrays;

public class MySerializable implements Serializable {
    private int int1;
    private int int2;
    private double dbl1;
    private String str1;
    //private String[] str2 = {"a", "ab", "abc"};

    public MySerializable() {}

    public MySerializable(int int1, int int2, double dbl1, String str1) {
        this.int1 = int1;
        this.int2 = int2;
        this.dbl1 = dbl1;
        this.str1 = str1;
    }

    @Override
    public String toString() {
        return "MySerializable{" +
                "int1=" + int1 +
                ", int2=" + int2 +
                ", dbl1=" + dbl1 +
                ", str1='" + str1 + '\'' +
                //", str2=" + Arrays.toString(str2) +
                '}';
    }
}
